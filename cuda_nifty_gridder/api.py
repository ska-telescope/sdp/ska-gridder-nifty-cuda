# this try/except is to allow sphinx to parse this file for static documentation when 
# cuda_nifty_gridder has not been installed, however this then causes coverage issues 
# for the except, so that is now ignored.
try:
    import _cuda_nifty_gridder_lib
except ImportError:  # pragma: no cover
    _cuda_nifty_gridder_lib = None

def ms2dirty(uvw, freq, ms, weight, npix_x, npix_y, pixsize_x_rad, pixsize_y_rad, dummy1, dummy2, epsilon, do_wstacking=True, verbosity = 0):
	"""
	Converts visibilities to a dirty image.

	Parameters
	==========
	uvw: numpy.array((nrows, 3), dtype=numpy.float32 or numpy.float64)
		(u,v,w) coordinates.
	freq: numpy.array((nchan,))
		Channel frequencies.
	ms: numpy.array((nrows, nchan), dtype=numpy.complex64 or numpy.complex128)
		The input measurement set data.
		Its data type determines the precision used for the gridding.
	weight: numpy.array((nrows, nchan), same precision as **ms**), optional
		If present, its values are used to multiply the input.
	npix_x: int 
		x dimension of the dirty image (see Note above).
	npix_y: int
		y dimension of the dirty image (see Note above).
	pixsize_x_rad: float
		Angular x pixel size (in radians) of the dirty image (see Note above).
	pixsize_y_rad: float
		Angular y pixel size (in radians) of the dirty image (see Note above).
	dummy1: int
		Ignored, just here for compatablity with ducc Nifty Gridder
	dummy2: int
		Ignored, just here for compatablity with ducc Nifty Gridder
	epsilon: float
		Accuracy at which the computation should be done.
		Must be larger than 2e-13.
		If **ms** has type numpy.complex64, it must be larger than 1e-5.
	do_wstacking: bool
		If True, the full improved w-stacking algorithm is carried out,
		otherwise the w values are assumed to be zero.

	Returns
	=======
	numpy.array((nxdirty, nydirty), dtype=float of same precision as **ms**)
		The dirty image.
	"""
	# pylint: disable=c-extension-no-member
	dirty = _cuda_nifty_gridder_lib.ms2dirty(uvw, freq, ms, weight, npix_x, npix_y, pixsize_x_rad, pixsize_y_rad, epsilon, do_wstacking, verbosity)

	return dirty
	
def dirty2ms(uvw, freq, dirty, weight, pixsize_x_rad, pixsize_y_rad, dummy1, dummy2, epsilon, do_wstacking=True, verbosity = 0):
	"""
	Converts a dirty image to visibilities.

	Parameters
	==========
	uvw: numpy.array((nrows, 3), dtype=numpy.float32 or numpy.float64)
		(u,v,w) coordinates.
	freq: numpy.array((nchan,))
		Channel frequencies.
	dirty: numpy.array((npix_x, npix_y), dtype=numpy.float32 or numpy.float64)
		The dirty image.
		Its data type determines the precision used for the degridding.
	weight: numpy.array((nrows, nchan), same precision as **dirty**), optional
		If present, its values are used to multiply the input.
	pixsize_x_rad: float
		Angular x pixel size (in radians) of the dirty image (see Note above).
	pixsize_y_rad: float
		Angular y pixel size (in radians) of the dirty image (see Note above).
	dummy1: int
		Ignored, just here for compatablity with ducc Nifty Gridder
	dummy2: int
		Ignored, just here for compatablity with ducc Nifty Gridder
	epsilon: float
		Accuracy at which the computation should be done.
		Must be larger than 2e-13.
		If **dirty** has type numpy.float32, it must be larger than 1e-5.
	do_wstacking: bool
		If True, the full improved w-stacking algorithm is carried out,
		otherwise the w values are assumed to be zero.

	Returns
	=======
	numpy.array((nrows, nchan), dtype=complex of same precision as **dirty**)
		The visibility data.
	"""

	# pylint: disable=c-extension-no-member
	ms = _cuda_nifty_gridder_lib.dirty2ms(uvw, freq, dirty, weight, pixsize_x_rad, pixsize_y_rad, epsilon, do_wstacking, verbosity)
	
	return ms
