
// Copyright 2021 Adam Campbell, Anthony Griffin, Andrew Ensor, Seth Hall
// Copyright 2021 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef CONTROLLER_H_
#define CONTROLLER_H_  

    #include <iostream>
    #include <string>
    #include <sstream>

    #include <cuda.h>
	#include <cuda_runtime_api.h>
    #include <cuComplex.h>
    #include <cufft.h>
    #include <cuda_fp16.h>

#ifdef __cplusplus
extern "C" {
#endif

    #include <stdio.h>
    #include <stdarg.h>
    #include <stdlib.h>
    #include <stdint.h>
    #include <stdbool.h>
    #include <string.h>
    #include <math.h>
    #include <time.h>
    #include <float.h>

    #define MIN(a,b) (((a)<(b))?(a):(b))
    #define MAX(a,b) (((a)>(b))?(a):(b))

    #define C 299792458.0

    // Use half precision computation/buffers
    #ifndef NIFTY_HALF
        #define NIFTY_HALF 0 // TODO
    #endif

    // Use single precision computation/buffers
    #ifndef NIFTY_SINGLE
        #define NIFTY_SINGLE 1
    #endif

    // Use double precision computation/buffers
    #ifndef NIFTY_DOUBLE
        #define NIFTY_DOUBLE 2
    #endif

    // What precision to use for general computation/buffers
    // NOTE: only supports single/double
    #ifndef NIFTY_PRECISION
        #define NIFTY_PRECISION NIFTY_SINGLE
        //#define NIFTY_PRECISION NIFTY_DOUBLE
    #endif

    // What precision to use for buffering/processing visibilities
    // NOTE: supports half/single/double
    #ifndef VISIBILITY_PRECISION
        #define VISIBILITY_PRECISION NIFTY_SINGLE
        //#define VISIBILITY_PRECISION NIFTY_DOUBLE
    #endif

    #if NIFTY_PRECISION == NIFTY_SINGLE
        #define PRECISION float
        #define PRECISION2 float2
        #define PRECISION3 float3
        #define PRECISION4 float4
        #define PRECISION_MAX FLT_MAX
        #define PI ((float) 3.141592654)
        #define CUFFT_C2C_PLAN CUFFT_C2C
        #define CUFFT_C2P_PLAN CUFFT_C2R
    #else // NIFTY_PRECISION == NIFTY_DOUBLE
        #define PRECISION double
        #define PRECISION2 double2
        #define PRECISION3 double3
        #define PRECISION4 double4
        #define PRECISION_MAX DBL_MAX
        #define PI ((double) 3.1415926535897931)
        #define CUFFT_C2C_PLAN CUFFT_Z2Z
        #define CUFFT_C2P_PLAN CUFFT_Z2D
    #endif

    #if NIFTY_PRECISION == NIFTY_SINGLE
        #define SIN(x) sinf(x)
        #define COS(x) cosf(x)
        #define SINCOS(x, y, z) sincosf(x, y, z)
        #define ABS(x) fabsf(x)
        #define SQRT(x) sqrtf(x)
        #define ROUND(x) roundf(x)
        #define CEIL(x) ceilf(x)
        #define LOG(x) logf(x)
        #define POW(x, y) powf(x, y)
        #define FLOOR(x) floorf(x)
        #define MAKE_PRECISION2(x,y) make_float2(x,y)
        #define MAKE_PRECISION3(x,y,z) make_float3(x,y,z)
        #define MAKE_PRECISION4(x,y,z,w) make_float4(x,y,z,w)
        #define CUFFT_EXECUTE_C2P(a,b,c) cufftExecC2R(a,b,c)
        #define CUFFT_EXECUTE_C2C(a,b,c,d) cufftExecC2C(a,b,c,d)
    #else // NIFTY_PRECISION == NIFTY_DOUBLE
        #define SIN(x) sin(x)
        #define COS(x) cos(x)
        #define SINCOS(x, y, z) sincos(x, y, z)
        #define ABS(x) fabs(x)
        #define SQRT(x) sqrt(x)
        #define ROUND(x) round(x)
        #define CEIL(x) ceil(x)
        #define LOG(x) log(x)
        #define POW(x, y) pow(x, y)
        #define FLOOR(x) floor(x)
        #define MAKE_PRECISION2(x,y) make_double2(x,y)
        #define MAKE_PRECISION3(x,y,z) make_double3(x,y,z)
        #define MAKE_PRECISION4(x,y,z,w) make_double4(x,y,z,w)
        #define CUFFT_EXECUTE_C2P(a,b,c) cufftExecZ2D(a,b,c)
        #define CUFFT_EXECUTE_C2C(a,b,c,d) cufftExecZ2Z(a,b,c,d)
    #endif

    // Defines the precision of visibility buffers and computation
    #if VISIBILITY_PRECISION == NIFTY_HALF
        #define VIS_PRECISION __half
        #define VIS_PRECISION2 __half2
        #define MAKE_VIS_PRECISION2(x,y) make_half2(x,y)
        #define VEXP(x)  hexp(x)
        #define VSQRT(x) hsqrt(x)
        #define VDISP(x) __half2float(x)
    #elif VISIBILITY_PRECISION == NIFTY_SINGLE
        #define VIS_PRECISION float
        #define VIS_PRECISION2 float2
        #define MAKE_VIS_PRECISION2(x,y) make_float2(x,y)
        #define VEXP(x)  expf(x)
        #define VSQRT(x) sqrtf(x)
        #define VDISP(x) (x)
    #else // VISIBILITY_PRECISION == NIFTY_DOUBLE
        #define VIS_PRECISION double
        #define VIS_PRECISION2 double2
        #define MAKE_VIS_PRECISION2(x,y) make_double2(x,y)
        #define VEXP(x)  exp(x)
        #define VSQRT(x) sqrt(x)
        #define VDISP(x) (x)
    #endif

    // bound above the maximum possible support for use in nifty_gridding kernel when precalculating kernel values
    #define KERNEL_SUPPORT_BOUND 16

    // bound for maximum possible samples for storing precalculated gauss-legendre quadrature nodes, weights, kernel
    // value should ideally be nextPowerOf2(p), where p = (1.5*config->support+2)
    // For more info on value of p, see first paragraph under equation 3.10, page 8 of paper:
    // A parallel non-uniform fast Fourier transform library based on an "exponential of semicircle" kernel
    #define QUADRATURE_SUPPORT_BOUND 32

    // Number of iterations to perform for more precise approximation of legendre root
    #define MAX_NEWTON_RAPHSON_ITERATIONS 100

    typedef struct config {
		int log_level; // verbosity for logging (currently just to screen)
        PRECISION fov; // field of view in degrees
        PRECISION freq; // single freq in hertz
        uint32_t num_channels; // number of total channels (currently single chan only)
        uint32_t num_visibilities; // How many visibilities to process
        uint32_t image_size; // number of pixels in one dimension for dirty image
        PRECISION min_abs_w; // minimum absolute w coordinate across all vis (non negative)
        PRECISION max_abs_w; // maximum absolute w coordinate across all vis
        uint32_t num_w_grids_batched; // How many w grids to grid at once on the GPU
        char data_output_folder[129]; // Define where output files to be saved, ie: dirty image, intermediate w grids (testing only)
        char vis_intensity_file[129]; // File path to locate input visibility intensities
        char vis_uvw_file[129]; // File path to locate input visibility coords (uvw)
        bool generating_psf; // uses nifty gridder with visibilities of 1.0+0.0i
        bool perform_shift_fft;  // flag to (equivalently) rearrange each grid so origin is at lower-left corner for FFT

        uint32_t grid_size;  // upscaled one dimension size of grid for each w grid
        PRECISION pixel_size; // converts pixel index (x, y) to normalised image coordinate (l, m) where l, m between -0.5 and 0.5
        PRECISION uv_scale; // scaling factor for conversion of uv coords to grid coordinates (grid_size * cell_size)
        PRECISION w_scale; // scaling factor for converting w coord to signed w grid index
        PRECISION min_plane_w; // w coordinate of minimum w plane
        PRECISION max_plane_w; // w coordinate of maximum w plane
        PRECISION alpha;
        VIS_PRECISION beta;  // NOTE this 2.30 is only for when upsampling = 2
        PRECISION upsampling; // sigma, the scaling factor for padding each w grid (image_size * upsampling)
        uint32_t num_total_w_grids; // Total number of w grids 
        uint32_t support; // full support for semicircle gridding kernel
    } config;

    typedef struct host_memory_handles {
        PRECISION      *dirty_image;
        PRECISION2     *w_grid_stack;
        VIS_PRECISION2 *visibilities;
        VIS_PRECISION  *vis_weights;
        PRECISION3     *uvw_coords;
        PRECISION      *quadrature_nodes;
        PRECISION      *quadrature_weights;
        PRECISION      *quadrature_kernel;
        PRECISION      *conv_corr_kernel;
    } host_memory_handles;

    typedef struct device_memory_handles {
        PRECISION      *d_dirty_image;
        PRECISION2     *d_w_grid_stack;
        VIS_PRECISION2 *d_visibilities;
        VIS_PRECISION  *d_vis_weights;
        PRECISION3     *d_uvw_coords;
        PRECISION      *d_conv_corr_kernel;
        cufftHandle    *fft_plan;
    } device_memory_handles;

    #define CUDA_CHECK_RETURN(value) check_cuda_error_aux(__FILE__,__LINE__, #value, value)

    #define CUFFT_SAFE_CALL(err) cufft_safe_call(err, __FILE__, __LINE__)
    
    int execute_imaging_pipeline(config *config);

    void init_memory_handles(host_memory_handles *host, device_memory_handles *device);

    void allocate_host_bound_buffers(host_memory_handles *host, device_memory_handles *device, config *config);

    void clean_up_memory_handles(host_memory_handles *host, device_memory_handles *device);

    void perform_nifty_gridding_degridding(host_memory_handles *host, device_memory_handles *device, config *config);

    void nifty_init(host_memory_handles *host, device_memory_handles *device, config *config);

    void nifty_cleanup(device_memory_handles *device);

    void generate_gauss_legendre_conv_kernel(host_memory_handles *host, config *config);

    void generate_synthetic_visibilities(host_memory_handles *host, config *config);

    void load_visibilities_from_file(host_memory_handles *host, config *config);

    void save_predicted_visibilities(host_memory_handles *host, config *config);

    void copy_dirty_image_to_host(  host_memory_handles *host, device_memory_handles *device, config *config);
	void copy_dirty_image_to_device(host_memory_handles *host, device_memory_handles *device, config *config);

    void save_image_to_file( host_memory_handles *host, config *config, int cycle_number);
    void save_image2_to_file(host_memory_handles *host, config *config, int cycle_number);

    void proof_of_concept_copy_w_grid_stack_to_host(host_memory_handles *host, device_memory_handles *device, config *config);

    void proof_of_concept_save_w_grids_to_file( host_memory_handles *host, config *config, uint32_t start_w_grid_index);
    void proof_of_concept_save_w_images_to_file(host_memory_handles *host, config *config, uint32_t start_w_grid_index);

    double get_legendre(double x, int n, double *derivative);

    double get_approx_legendre_root(int32_t i, int32_t n);

    double calculate_legendre_root(int32_t i, int32_t n, double accuracy, double *weight);

    void check_cuda_error_aux(const char *file, uint32_t line, const char *statement, cudaError_t err);

    void cufft_safe_call(cufftResult err, const char *file, const int line);

    const char* cuda_get_error_enum(cufftResult error);

	void LogF(config *config, int thisLevel, const char * format, ... );

#ifdef __cplusplus
}
#endif

#endif /* CONTROLLER_H_ */
