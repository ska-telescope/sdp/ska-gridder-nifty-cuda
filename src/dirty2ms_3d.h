/* See the LICENSE file at the top-level directory of this distribution. */

#ifndef DIRTY2MS_3D_H_
#define DIRTY2MS_3D_H_

#include "wrap_gpu.h"

#ifdef __cplusplus
extern "C" {
#endif

void dirty2ms_3d(WrapCUDA& wrapper, int num_rows, int num_chan, const Mem* uvw, const Mem* freq_hz,
        const Mem* im, const Mem* weight, int npix_x, int npix_y,
        double pixsize_x_rad, double pixsize_y_rad, double epsilon,
        Mem* ms, int verbosityLevel, int* status);

#ifdef __cplusplus
}
#endif

#endif /* include guard */
