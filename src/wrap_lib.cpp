/* See the LICENSE file at the top-level directory of this distribution. */

#include <cstdio>
#include <cstdlib>
#include <cstring>

#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__))
#define WRAP_LIB_OS_WIN32
#endif
#if (defined(WIN64) || defined(_WIN64) || defined(__WIN64__))
#define WRAP_LIB_OS_WIN64
#endif

#if (defined(WRAP_LIB_OS_WIN32) || defined(WRAP_LIB_OS_WIN64))
#define WRAP_LIB_OS_WIN
#endif

#if defined(WRAP_LIB_OS_WIN)
#include <Windows.h>
#else
#include <dlfcn.h>
#endif

#include "wrap_lib.h"

void* lib_open(const char* name)
{
    void* handle;
    char* full_name;
    full_name = (char*) calloc(7 + strlen(name), sizeof(char));
#if defined(WRAP_LIB_OS_WIN)
    sprintf(full_name, "%s.dll", name);
    handle = LoadLibrary(full_name);
#elif defined(__APPLE__)
    sprintf(full_name, "%s.dylib", name);
    handle = dlopen(full_name, RTLD_NOW);
#else
    sprintf(full_name, "%s.so", name);
    handle = dlopen(full_name, RTLD_NOW);
#endif
    free(full_name);
    return handle;
}

int lib_close(void* handle)
{
    if (!handle) return 0;
#if defined(WRAP_LIB_OS_WIN)
    return FreeLibrary((HMODULE)handle);
#else
    return dlclose(handle);
#endif
}

void* lib_symbol(void* handle, const char* symbol_name)
{
    void* symbol;
    if (!handle)
    {
        fprintf(stderr, "Invalid library handle\n");
        return 0;
    }
#if defined(WRAP_LIB_OS_WIN)
    symbol = (void*)GetProcAddress((HMODULE)handle, symbol_name);
#else
    symbol = dlsym(handle, symbol_name);
#endif
    if (!symbol)
        fprintf(stderr, "Could not resolve symbol '%s'\n", symbol_name);
    return symbol;
}
