/* See the LICENSE file at the top-level directory of this distribution. */

#include <Python.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include "dirty2ms_2d.h"
#include "dirty2ms_3d.h"
#include "ms2dirty_2d.h"
#include "ms2dirty_3d.h"
#include "wrap_gpu.h"

static const char module_doc[] =
        "Provides a public interface to the CUDA Nifty Gridder.  Same interface as DUCC/nifty gridder, by Martin Reinecke.";

static int mem_type_from_numpy(PyArrayObject* arr)
{
    switch (PyArray_TYPE(arr))
    {
    case NPY_INT:     return MEM_INT;
    case NPY_FLOAT:   return MEM_FLOAT;
    case NPY_DOUBLE:  return MEM_DOUBLE;
    case NPY_CFLOAT:  return MEM_COMPLEX_FLOAT;
    case NPY_CDOUBLE: return MEM_COMPLEX_DOUBLE;
    }
    return 0;
}

static const char ms2dirty_doc[] = R"(
ms2dirty(uvw, freq, ms, weight, npix_x, npix_y, pixsize_x_rad, pixsize_y_rad, epsilon, do_wstacking)

Converts visibilities to a dirty image.

Parameters
==========
uvw: numpy.array((nrows, 3), dtype=numpy.float32 or numpy.float64)
    (u,v,w) coordinates.
freq: numpy.array((nchan,))
    Channel frequencies.
ms: numpy.array((nrows, nchan), dtype=numpy.complex64 or numpy.complex128)
    The input measurement set data.
    Its data type determines the precision used for the gridding.
weight: numpy.array((nrows, nchan), same precision as `ms`), optional
    If present, its values are used to multiply the input.
npix_x: int 
    x dimension of the dirty image (see Note above).
npix_y: int
    y dimension of the dirty image (see Note above).
pixsize_x_rad: float
    Angular x pixel size (in radians) of the dirty image (see Note above).
pixsize_y_rad: float
    Angular y pixel size (in radians) of the dirty image (see Note above).
epsilon: float
    Accuracy at which the computation should be done.
    Must be larger than 2e-13.
    If `ms` has type numpy.complex64, it must be larger than 1e-5.
do_wstacking: bool
    If True, the full improved w-stacking algorithm is carried out,
    otherwise the w values are assumed to be zero.

Returns
=======
numpy.array((nxdirty, nydirty), dtype=float of same precision as `ms`)
    The dirty image.
)";

static PyObject* ms2dirty_py(PyObject* self, PyObject* args)
{
    WrapCUDA wrapper;
    int error = wrapper.cuCtxSetCurrent(wrapper.context_);
    if(error) fprintf(stderr, "cuCtxSetCurrent error: %i", error);
    error = wrapper.cuModuleLoadData(&wrapper.module_, wrapper.ptx_);
    if(error) fprintf(stderr, "cuModuleLoadData error: %i", error);

    int status = 0;
    PyObject *obj[] = {0, 0, 0, 0};
    PyArrayObject *uvw = 0, *freq = 0, *ms = 0, *weight = 0, *im = 0;
    Mem *uvw_ = 0, *freq_ = 0, *ms_ = 0, *weight_ = 0, *im_ = 0;
    int npix_x = 0, npix_y = 0, do_wstacking = 0, double_precision = 0;
    int num_ms_dims = 0, num_rows = 0, num_chan = 0, verbosityLevel = 0;
    double pixsize_x_rad = 0.0, pixsize_y_rad = 0.0, epsilon = 0.0;
    npy_intp image_dims[2], *ms_dims, *freq_dims;

    /* Parse inputs. */
    if (!PyArg_ParseTuple(args, "OOOOiidddii",
            &obj[0], &obj[1], &obj[2], &obj[3], &npix_x, &npix_y,
            &pixsize_x_rad, &pixsize_y_rad, &epsilon, &do_wstacking, &verbosityLevel))
        return 0;

    /* Make sure input objects are arrays. Convert if required. */
    uvw   = (PyArrayObject*) PyArray_FROM_OF(obj[0], NPY_ARRAY_IN_ARRAY);
    ms    = (PyArrayObject*) PyArray_FROM_OF(obj[2], NPY_ARRAY_IN_ARRAY);
    freq  = (PyArrayObject*) PyArray_FROM_OTF(obj[1],
            PyArray_TYPE(uvw), NPY_ARRAY_IN_ARRAY | NPY_ARRAY_FORCECAST);
    if (!uvw || !freq || !ms) goto fail;

    /* Get and check dimensions of input arrays. */
    num_ms_dims = PyArray_NDIM(ms);
    if (num_ms_dims != 2)
    {
        PyErr_Format(PyExc_RuntimeError,
                "Visibility data array must be two-dimensional.");
        goto fail;
    }
    ms_dims = PyArray_DIMS(ms);
    freq_dims = PyArray_DIMS(freq);
    num_rows = ms_dims[0];
    num_chan = ms_dims[1];
    if (freq_dims[0] != num_chan)
    {
        PyErr_Format(PyExc_RuntimeError,
                "Inconsistent frequency dimensions (got %d; expected %d).",
                freq_dims[0], num_chan);
        goto fail;
    }

    /* Check if weights are present. */
    if (obj[3] != Py_None)
    {
        weight = (PyArrayObject*) PyArray_FROM_OF(obj[3], NPY_ARRAY_IN_ARRAY);
        if (!weight) goto fail;
    }

    /* Get precision of visibility data. */
    switch (PyArray_TYPE(ms))
    {
    case NPY_CFLOAT:
        double_precision = 0;
        break;
    case NPY_CDOUBLE:
        double_precision = 1;
        break;
    default:
        PyErr_Format(PyExc_RuntimeError,
                "Visibilities must be single complex or double complex.");
        goto fail;
    }

    /* Create the output image. */
    image_dims[0] = npix_x;
    image_dims[1] = npix_y;
    im = (PyArrayObject*)PyArray_SimpleNew(2, image_dims,
            double_precision ? NPY_DOUBLE : NPY_FLOAT);
				
	// AG the following is used to output a complex grid
    // image_dims[0] = npix_x*2;
    // image_dims[1] = npix_y*2;
    // im = (PyArrayObject*)PyArray_SimpleNew(2, image_dims,
            // double_precision ? NPY_CDOUBLE : NPY_CFLOAT);
	
    /* Wrap Python arrays as Mem. */
    uvw_ = mem_create_alias_from_raw(
            PyArray_DATA(uvw), mem_type_from_numpy(uvw), MEM_CPU,
            PyArray_SIZE(uvw), &status);
    freq_ = mem_create_alias_from_raw(
            PyArray_DATA(freq), mem_type_from_numpy(freq), MEM_CPU,
            PyArray_SIZE(freq), &status);
    ms_ = mem_create_alias_from_raw(
            PyArray_DATA(ms), mem_type_from_numpy(ms), MEM_CPU,
            PyArray_SIZE(ms), &status);
    im_ = mem_create_alias_from_raw(
            PyArray_DATA(im), mem_type_from_numpy(im), MEM_CPU,
            PyArray_SIZE(im), &status);
    if (weight)
    {
        weight_ = mem_create_alias_from_raw(
                PyArray_DATA(weight), mem_type_from_numpy(weight), MEM_CPU,
                PyArray_SIZE(weight), &status);
    }
    else
    {
        /* Create unity weights if necessary. */
        int num_elements = num_rows * num_chan;
        weight_ = wrapper.mem_create((double_precision ? MEM_DOUBLE : MEM_FLOAT),
                MEM_CPU, num_elements, &status);
        if (double_precision)
        {
            double* temp = (double*) mem_ptr(weight_);
            for (int i = 0; i < num_elements; ++i)
                temp[i] = 1.0;
        }
        else
        {
            float* temp = (float*) mem_ptr(weight_);
            for (int i = 0; i < num_elements; ++i)
                temp[i] = 1.0;
        }
    }

    /* Grid visibilities. */
    if (do_wstacking)
    {
        Py_BEGIN_ALLOW_THREADS
        ms2dirty_3d(wrapper, num_rows, num_chan, uvw_, freq_, ms_, weight_,
                npix_x, npix_y, pixsize_x_rad, pixsize_y_rad, epsilon, im_, verbosityLevel,
                &status);
        Py_END_ALLOW_THREADS
    }
    else
    {
        Py_BEGIN_ALLOW_THREADS
        ms2dirty_2d(wrapper, num_rows, num_chan, uvw_, freq_, ms_, weight_,
                npix_x, npix_y, pixsize_x_rad, pixsize_y_rad, epsilon, im_, verbosityLevel,
                &status);
        Py_END_ALLOW_THREADS
    }

    /* Check for errors. */
    if (status)
    {
        PyErr_Format(PyExc_RuntimeError, "ms2dirty() failed (code %d).", status);
        goto fail;
    }

    /* Return image to Python. */
    wrapper.mem_free(uvw_, &status);
    wrapper.mem_free(freq_, &status);
    wrapper.mem_free(ms_, &status);
    wrapper.mem_free(weight_, &status);
    wrapper.mem_free(im_, &status);
    Py_XDECREF(uvw);
    Py_XDECREF(freq);
    Py_XDECREF(ms);
    Py_XDECREF(weight);
    return Py_BuildValue("N", im);

fail:
    wrapper.mem_free(uvw_, &status);
    wrapper.mem_free(freq_, &status);
    wrapper.mem_free(ms_, &status);
    wrapper.mem_free(weight_, &status);
    wrapper.mem_free(im_, &status);
    Py_XDECREF(uvw);
    Py_XDECREF(freq);
    Py_XDECREF(ms);
    Py_XDECREF(weight);
    Py_XDECREF(im);
    return 0;
}


static const char dirty2ms_doc[] = R"(
dirty2ms(uvw, freq, dirty, weight, pixsize_x_rad, pixsize_y_rad, epsilon, do_wstacking)

Converts a dirty image to visibilities.

Parameters
==========
uvw: numpy.array((nrows, 3), dtype=numpy.float32 or numpy.float64)
    (u,v,w) coordinates.
freq: numpy.array((nchan,))
    Channel frequencies.
dirty: numpy.array((npix_x, npix_y), dtype=numpy.float32 or numpy.float64)
    The dirty image.
    Its data type determines the precision used for the degridding.
weight: numpy.array((nrows, nchan), same precision as `ms`), optional
    If present, its values are used to multiply the input.
pixsize_x_rad: float
    Angular x pixel size (in radians) of the dirty image (see Note above).
pixsize_y_rad: float
    Angular y pixel size (in radians) of the dirty image (see Note above).
epsilon: float
    Accuracy at which the computation should be done.
    Must be larger than 2e-13.
    If `dirty` has type numpy.float32, it must be larger than 1e-5.
do_wstacking: bool
    If True, the full improved w-stacking algorithm is carried out,
    otherwise the w values are assumed to be zero.

Returns
=======
numpy.array((nrows, nchan), dtype=complex of same precision as `dirty`)
    The visibility data.
)";

static PyObject* dirty2ms_py(PyObject* self, PyObject* args)
{
    WrapCUDA wrapper;
    int error = wrapper.cuCtxSetCurrent(wrapper.context_);
    if(error) fprintf(stderr, "cuCtxSetCurrent error: %i", error);
    error = wrapper.cuModuleLoadData(&wrapper.module_, wrapper.ptx_);
    if(error) fprintf(stderr, "cuModuleLoadData error: %i", error);

    int status = 0;
    PyObject *obj[] = {0, 0, 0, 0};
    PyArrayObject *uvw = 0, *freq = 0, *ms = 0, *weight = 0, *im = 0;
    Mem *uvw_ = 0, *freq_ = 0, *ms_ = 0, *weight_ = 0, *im_ = 0;
    int npix_x = 0, npix_y = 0, do_wstacking = 0, double_precision = 0;
    int num_rows = 0, num_chan = 0, verbosityLevel = 0;
    double pixsize_x_rad = 0.0, pixsize_y_rad = 0.0, epsilon = 0.0;
    npy_intp ms_dims[2], *image_dims, *uvw_dims, *freq_dims;

    /* Parse inputs. */
    if (!PyArg_ParseTuple(args, "OOOOdddii",
            &obj[0], &obj[1], &obj[2], &obj[3],
            &pixsize_x_rad, &pixsize_y_rad, &epsilon, &do_wstacking, &verbosityLevel))
        return 0;

    /* Make sure input objects are arrays. Convert if required. */
    uvw   = (PyArrayObject*) PyArray_FROM_OF(obj[0], NPY_ARRAY_IN_ARRAY);
    im    = (PyArrayObject*) PyArray_FROM_OF(obj[2], NPY_ARRAY_IN_ARRAY);
    freq  = (PyArrayObject*) PyArray_FROM_OTF(obj[1],
            PyArray_TYPE(uvw), NPY_ARRAY_IN_ARRAY | NPY_ARRAY_FORCECAST);
    if (!uvw || !freq || !im) goto fail;

    /* Check if weights are present. */
    if (obj[3] != Py_None)
    {
        weight = (PyArrayObject*) PyArray_FROM_OF(obj[3], NPY_ARRAY_IN_ARRAY);
        if (!weight) goto fail;
    }

    /* Get and check dimensions of input arrays. */
    image_dims = PyArray_DIMS(im);
    uvw_dims = PyArray_DIMS(uvw);
    freq_dims = PyArray_DIMS(freq);
    npix_x = image_dims[0];
    npix_y = image_dims[1];
    num_rows = uvw_dims[0];
    num_chan = freq_dims[0];

    /* Get precision of image. */
    switch (PyArray_TYPE(im))
    {
    case NPY_FLOAT:
        double_precision = 0;
        break;
    case NPY_DOUBLE:
        double_precision = 1;
        break;
    default:
        PyErr_Format(PyExc_RuntimeError,
                "Image must be single or double precision.");
        goto fail;
    }

    /* Create the output visibility array. */
    ms_dims[0] = num_rows;
    ms_dims[1] = num_chan;
    ms = (PyArrayObject*)PyArray_SimpleNew(2, ms_dims,
            double_precision ? NPY_CDOUBLE : NPY_CFLOAT);

    /* Wrap Python arrays as Mem. */
    uvw_ = mem_create_alias_from_raw(
            PyArray_DATA(uvw), mem_type_from_numpy(uvw), MEM_CPU,
            PyArray_SIZE(uvw), &status);
    freq_ = mem_create_alias_from_raw(
            PyArray_DATA(freq), mem_type_from_numpy(freq), MEM_CPU,
            PyArray_SIZE(freq), &status);
    ms_ = mem_create_alias_from_raw(
            PyArray_DATA(ms), mem_type_from_numpy(ms), MEM_CPU,
            PyArray_SIZE(ms), &status);
    im_ = mem_create_alias_from_raw(
            PyArray_DATA(im), mem_type_from_numpy(im), MEM_CPU,
            PyArray_SIZE(im), &status);
    if (weight)
    {
        weight_ = mem_create_alias_from_raw(
                PyArray_DATA(weight), mem_type_from_numpy(weight), MEM_CPU,
                PyArray_SIZE(weight), &status);
    }
    else
    {
        /* Create unity weights if necessary. */
        int num_elements = num_rows * num_chan;
        weight_ = wrapper.mem_create((double_precision ? MEM_DOUBLE : MEM_FLOAT),
                MEM_CPU, num_elements, &status);
        if (double_precision)
        {
            double* temp = (double*) mem_ptr(weight_);
            for (int i = 0; i < num_elements; ++i)
                temp[i] = 1.0;
        }
        else
        {
            float* temp = (float*) mem_ptr(weight_);
            for (int i = 0; i < num_elements; ++i)
                temp[i] = 1.0;
        }
    }

    /* De-grid visibilities. */
    if (do_wstacking)
    {
        Py_BEGIN_ALLOW_THREADS
        dirty2ms_3d(wrapper, num_rows, num_chan, uvw_, freq_, im_, weight_,
                npix_x, npix_y, pixsize_x_rad, pixsize_y_rad, epsilon, ms_, verbosityLevel,
                &status);
        Py_END_ALLOW_THREADS
    }
    else
    {
        Py_BEGIN_ALLOW_THREADS
        dirty2ms_2d(wrapper, num_rows, num_chan, uvw_, freq_, im_, weight_,
                npix_x, npix_y, pixsize_x_rad, pixsize_y_rad, epsilon, ms_, verbosityLevel,
                &status);
        Py_END_ALLOW_THREADS
    }

    /* Check for errors. */
    if (status)
    {
        PyErr_Format(PyExc_RuntimeError, "dirty2ms() failed (code %d).", status);
        goto fail;
    }

    /* Return data to Python. */
    wrapper.mem_free(uvw_, &status);
    wrapper.mem_free(freq_, &status);
    wrapper.mem_free(ms_, &status);
    wrapper.mem_free(weight_, &status);
    wrapper.mem_free(im_, &status);
    Py_XDECREF(uvw);
    Py_XDECREF(freq);
    Py_XDECREF(im);
    Py_XDECREF(weight);
    return Py_BuildValue("N", ms);

fail:
    wrapper.mem_free(uvw_, &status);
    wrapper.mem_free(freq_, &status);
    wrapper.mem_free(ms_, &status);
    wrapper.mem_free(weight_, &status);
    wrapper.mem_free(im_, &status);
    Py_XDECREF(uvw);
    Py_XDECREF(freq);
    Py_XDECREF(im);
    Py_XDECREF(weight);
    Py_XDECREF(ms);
    return 0;
}


/* Method table. */
static PyMethodDef methods[] =
{
        {"ms2dirty", (PyCFunction)ms2dirty_py, METH_VARARGS, ms2dirty_doc},
        {"dirty2ms", (PyCFunction)dirty2ms_py, METH_VARARGS, dirty2ms_doc},
        {NULL, NULL, 0, NULL}
};

/* Module definition. */
static PyModuleDef moduledef = {
        PyModuleDef_HEAD_INIT,
        "__cuda_nifty_gridder_lib",            /* m_name */
        module_doc,         /* m_doc */
        -1,                 /* m_size */
        methods             /* m_methods */
};

/* Initialisation function. */
PyMODINIT_FUNC PyInit__cuda_nifty_gridder_lib(void)
{
    import_array();
    PyObject* m = PyModule_Create(&moduledef);
    return m;
}
