
// Copyright 2021 Adam Campbell, Anthony Griffin, Andrew Ensor, Seth Hall
// Copyright 2021 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef TESTS_H_
#define TESTS_H_  

#ifdef __cplusplus
extern "C" {
#endif

int test_imaging_pipeline(config *config);

PRECISION check_predicted_visibilities(host_memory_handles *host, device_memory_handles *device, config *config);
PRECISION check_dirty_image(           host_memory_handles *host, device_memory_handles *device, config *config);

PRECISION2* load_predicted_visibilities_from_file(host_memory_handles *host, config *config);

PRECISION*  load_test_dirty_image(config *config, const char* specifier);
PRECISION2* load_test_grid(       config *config, const char* specifier);

void run_system_test(host_memory_handles *host, device_memory_handles *device, config *config);

void setup_for_HIPPO_data(config *config);

void test_conv_corr_and_scaling(host_memory_handles *host, device_memory_handles *device, config *config);
void test_nifty_gridding(       host_memory_handles *host, device_memory_handles *device, config *config);

PRECISION RRMSE2(PRECISION2 *est, PRECISION2 *val, uint32_t N);
PRECISION RRMSE( PRECISION*  est, PRECISION*  val, uint32_t N);

#ifdef __cplusplus
}
#endif 

#endif /* TESTS_H_ */
