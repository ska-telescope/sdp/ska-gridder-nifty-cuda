CUDA Nifty Gridder
==================
[![Documentation Status](https://readthedocs.org/projects/ska-gridder-nifty-cuda/badge/?version=latest)](https://ska-gridder-nifty-cuda.readthedocs.io/en/latest/?badge=latest)

CUDA Nifty Gridder is a GPU-accelerated implementation of [DUCC/Nifty Gridder](https://gitlab.mpcdf.mpg.de/mtr/ducc).

Requirements
------------

The system used for development must have an CUDA capable GPU and Python 3 and `pip` installed, along with the Python packages `numpy` and `setuptools`.

To run the unit tests [the DUCC version of Nifty Gridder](https://gitlab.mpcdf.mpg.de/mtr/ducc) must also be installed.

Installation
------------

From the root directory of the CUDA Nifty Gridder git repo, run:
```bash
$ pip3 install --user .
```

Alternatively, assuming `git` is installed, this can also be done with:
```bash
$ pip3 install 'git+https://gitlab.com/ska-telescope/sdp/ska-gridder-nifty-cuda.git'
```

This is also an easy way to install the reference [Nifty Gridder](https://gitlab.mpcdf.mpg.de/mtr/ducc) in order to run the unit tests.
```bash
$ pip3 install 'git+https://gitlab.mpcdf.mpg.de/mtr/ducc.git'
```

Usage
-----

CUDA Nifty Gridder can then be used in your Python code as:
```python
import cuda_nifty_gridder as cng 
```

See the files `test_ms2dirty.py` and `test_dirty2ms.py` in `tests/unit/` for more ideas on how to use CUDA Nifty Gridder.

The full documentation is available at [ReadTheDocs](https://developer.skao.int/projects/ska-gridder-nifty-cuda/en/latest/) and the [GitLab Page](https://ska-telescope.gitlab.io/sdp/ska-gridder-nifty-cuda)

Unit Tests
----------

If [PyTest](https://pytest.org) is installed, the unit tests can be run from the root directory of the repo as:
```bash
$ py.test -s
```
the `-s` provides feedback during the tests, and can be omitted.

Otherwise the tests can be run as:
```bash
$ python3 test.py
```
