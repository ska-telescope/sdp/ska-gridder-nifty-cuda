.. doctest-skip-all
.. _package-guide:

.. todo::
    - Insert todo's here

************************
Public API Documentation
************************

This section lists the functions implemented, using the same interface as DUCC/nifty gridder, by Martin Reinecke.

.. note::

   * Only square images are supported at the moment, so, **pixsize_x_rad** and **pixsize_y_rad** must be equal, and **npix_x** and **npix_y** must be equal.

Functions
=========

.. autofunction:: cuda_nifty_gridder.ms2dirty
.. autofunction:: cuda_nifty_gridder.dirty2ms 

