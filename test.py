import tests.unit.test_ms2dirty as tm2d
import tests.unit.test_dirty2ms as td2m
##import time

print()
print("Running unit tests...")
print()


tm2d.test_ms2dirty_single_2D()
tm2d.test_ms2dirty_double_2D()
tm2d.test_ms2dirty_single_3D()
tm2d.test_ms2dirty_double_3D()

td2m.test_dirty2ms_single_2D()
td2m.test_dirty2ms_double_2D()
td2m.test_dirty2ms_single_3D()
td2m.test_dirty2ms_double_3D()

print()
