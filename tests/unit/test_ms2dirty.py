import concurrent.futures
import numpy as np
import cuda_nifty_gridder as cng 
import ducc0.wgridder as rng

def RRMSE(est, actual):
	return np.linalg.norm(est - actual)/np.linalg.norm(actual)

#def compareCNGandRNG(uvw, freqs, vis, weights, imSize, pixsize_rad, epsilon):

#	#print("")
#	#print("Running CNG ms2dirty()...")
#	#print("")
#	cngDirty = cng.ms2dirty(uvw, freqs, vis, weights, imSize, imSize, pixsize_rad, pixsize_rad, epsilon, True)
#	#print("")
#	#print("Running RNG ms2dirty()...")
#	#print("")
#	rngDirty = rng.ms2dirty(uvw, freqs, vis, weights, imSize, imSize, pixsize_rad, pixsize_rad, epsilon, True)

#	return RRMSE(cngDirty, rngDirty)

def run_ms2dirty(doSingle=True, multiChannel=True, epsilon=1e-5, doWStacking = True):
	CEND   = '\33[0m'
	CRED   = '\33[31m'
	CGREEN = '\33[92m'

	useReferenceCode = True

	# Read test visibilities.
	if multiChannel:
		test_data = np.load("vla_d.npz")
		strChannel = "multi  channel"
		idChannel = "MC"
	else:
		test_data = np.load("vla_d_3_chan.npz")
		strChannel = "single channel"
		idChannel = "SC"

	vis = test_data["vis"]
	freqs = test_data["freqs"]
	uvw = test_data["uvw"]

	#print("Shape of visibility array (num_rows, num_chan): {}".format(vis.shape))
	#print("Channel frequencies (Hz): {}".format(freqs))

	if doSingle:
		# Convert data to single precision.
		vis = vis.astype(np.complex64)
		weights = np.ones_like(vis, dtype=np.float32)
		strPrecision = "single"
		idPrecision = "SP"
	else:
		weights = np.ones_like(vis, dtype=np.float64)
		strPrecision = "double"
		idPrecision = "DP"

	if doWStacking:
		strGridderType = "3D"
	else:
		strGridderType = "2D"

	passThreshold = 1.5*epsilon

	imSize = 1024
	pixsize_deg = 1.94322419749866394E-02
	pixsize_rad = pixsize_deg * np.pi / 180.0

	strTestSource = "reference code"

	#print("")
	#print("Running test against %s with %s precision..." % (strTestSource, strPrecision))
	#print("")

	if useReferenceCode:
		testDirty = rng.ms2dirty(uvw, freqs, vis, weights, imSize, imSize, pixsize_rad, pixsize_rad, 0, 0, epsilon, doWStacking, verbosity = 0)
	else:
		filename = "tests/unit/test_data/test_dirty_image_%i_%s_%s.npy" % (imSize, idChannel, idPrecision)
		testDirty = np.load(filename)

	cngDirty = cng.ms2dirty(uvw, freqs, vis, weights, imSize, imSize, pixsize_rad, pixsize_rad, 0, 0, epsilon, doWStacking, verbosity = 0)

	error = RRMSE(cngDirty, testDirty)

	print("    ms2dirty() as %s precision, %s gridder, with %s data and ε = %.0e, RRMSE is: %e. " % (strPrecision, strGridderType, strChannel, epsilon, error), end = "");

	if error < passThreshold:
		print(CGREEN + "PASS" + CEND)
	else:
		print(CRED   + "FAIL" + CEND)

	return error, passThreshold

def test_ms2dirty_single_2D():

	#es = [3, 6, 9, 13]
	es = range(1, 6)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])
		error, passThreshold = run_ms2dirty(doSingle=True, multiChannel=True, epsilon=thisEps, doWStacking=False)
		assert(error < passThreshold)

def test_ms2dirty_single_3D():

	#es = [3, 6, 9, 13]
	es = range(1, 6)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])
		error, passThreshold = run_ms2dirty(doSingle=True, multiChannel=True, epsilon=thisEps, doWStacking=True)
		assert(error < passThreshold)

def test_ms2dirty_double_2D():

	#es = [3, 6, 9, 13]
	es = range(5, 13)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])
		error, passThreshold = run_ms2dirty(doSingle=False, multiChannel=True, epsilon=thisEps, doWStacking=False)
		assert(error < passThreshold)

def test_ms2dirty_double_3D():

	#es = [3, 6, 9, 13]
	es = range(5, 13)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])
		error, passThreshold = run_ms2dirty(doSingle=False, multiChannel=True, epsilon=thisEps, doWStacking=True)
		assert(error < passThreshold)

def test_ms2dirty_double_3D_threadsafe():

	#es = [3, 6, 9, 13]
	es = range(5, 13)

	print() # this line formats better when -s is used with pytest

	with concurrent.futures.ThreadPoolExecutor() as executor:
		futures = []
		for e in range(0, len(es)):
			thisEps = 10**(-es[e])
			f = executor.submit(run_ms2dirty, doSingle=False, multiChannel=True, epsilon=thisEps, doWStacking = True)
			futures.append(f)
		for f in futures:
			error, passThreshold = f.result()
			assert(error < passThreshold)
