import numpy as np
import cuda_nifty_gridder as cng 
import nifty_gridder as rng

def RRMSE(est, actual):
	return np.linalg.norm(est - actual)/np.linalg.norm(actual)

def compareCNGandRNG(uvw, freqs, vis, weights, imSize, pixsize_rad, epsilon):

	#print("")
	#print("Running CNG ms2dirty()...")
	#print("")
	cngDirty = cng.ms2dirty(uvw, freqs, vis, weights, imSize, imSize, pixsize_rad, pixsize_rad, epsilon, True)
	#print("")
	#print("Running RNG ms2dirty()...")
	#print("")
	rngDirty = rng.ms2dirty(uvw, freqs, vis, weights, imSize, imSize, pixsize_rad, pixsize_rad, epsilon, True)

	return RRMSE(cngDirty, rngDirty)

def gen_ms2dirty_test_data(doSingle=True, multiChannel=True, epsilon=1e-5):
	CEND   = '\33[0m'
	CRED   = '\33[31m'
	CGREEN = '\33[92m'

	# Read test visibilities.
	if multiChannel:
		test_data = np.load("vla_d.npz")
		strChannel = "multi  channel"
		idChannel = "MC"
	else:
		test_data = np.load("vla_d_3_chan.npz")
		strChannel = "single channel"
		idChannel = "SC"
	vis = test_data["vis"]
	freqs = test_data["freqs"]
	uvw = test_data["uvw"]

	#print("Shape of visibility array (num_rows, num_chan): {}".format(vis.shape))
	#print("Channel frequencies (Hz): {}".format(freqs))

	if doSingle:
		# Convert data to single precision.
		vis = vis.astype(np.complex64)
		weights = np.ones_like(vis, dtype=np.float32)
		passThreshold = 1e-6
		strPrecision = "single"
		idPrecision = "SP"
	else:
		weights = np.ones_like(vis, dtype=np.float64)
		passThreshold = 1e-14
		strPrecision = "double"
		idPrecision = "DP"

	imSize = 1024
	pixsize_deg = 1.94322419749866394E-02
	pixsize_rad = pixsize_deg * np.pi / 180.0

	print("Generating data for ms2dirty() as %s precision, with %s data and ε = %.0e" % (strPrecision, strChannel, epsilon));

	rngDirty = rng.ms2dirty(uvw, freqs, vis, weights, imSize, imSize, pixsize_rad, pixsize_rad, epsilon, True)

	filename = "test_dirty_image_%i_%s_%s" % (imSize, idChannel, idPrecision)

	print("    writing %s..." % (filename));

	np.save(filename,  rngDirty)

	return

def test_ms2dirty_single():
	error, passThreshold = run_ms2dirty(doSingle=True, multiChannel=False, epsilon=1e-5)
	assert(error < passThreshold)

def test_ms2dirty_double():
	error, passThreshold = run_ms2dirty(doSingle=False, multiChannel=False, epsilon=1e-5)
	assert(error < passThreshold)

def test_ms2dirty_single_mc():
	error, passThreshold = run_ms2dirty(doSingle=True, multiChannel=True, epsilon=1e-5)
	assert(error < passThreshold)

def test_ms2dirty_double_mc():
	error, passThreshold = run_ms2dirty(doSingle=False, multiChannel=True, epsilon=1e-5)
	assert(error < passThreshold)


gen_ms2dirty_test_data(doSingle=True, multiChannel=True, epsilon=1e-5)