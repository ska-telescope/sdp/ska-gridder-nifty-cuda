
// Copyright 2021 Adam Campbell, Anthony Griffin, Andrew Ensor, Seth Hall
// Copyright 2021 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "src/controller.h"
#include "src/tests.h"

int main(int argc, char **argv)
{
    config config; // Simple struct for holding algorithm configuration

	// process cmd line arguments
	if (argc > 1)
	{
		char *a = argv[1];
		int num = atoi(a);

		config.log_level = num;

		LogF(&config, 1, "config.log_level set to %i\n", config.log_level);

		if (argc == 3)
		{
			if (strcmp(argv[2],"test") == 0)
			{
				LogF(&config, 0, "\nRunning tests ");
			#if NIFTY_PRECISION == NIFTY_SINGLE
				LogF(&config, 0, "with single precision processing.\n");
			#else // VISIBILITY_PRECISION == NIFTY_DOUBLE
				LogF(&config, 0, "with double precision processing.\n");
			#endif

				test_imaging_pipeline(&config);
				return(0);
			}
			else
			{
				printf("\nERROR: command line argument [%s] not recognised.\n", argv[1]);
				return(-1);
			}
		}
	}
	else
	{
		config.log_level = 1;  // what should the default level be?
	}

	LogF(&config, 1, "Gridder starting");
    #if VISIBILITY_PRECISION == NIFTY_HALF
        LogF(&config, 1, " for half precision complex visibility data.\n");
    #elif VISIBILITY_PRECISION == NIFTY_SINGLE
        LogF(&config, 1, " for single precision complex visibility data.\n");
    #else // VISIBILITY_PRECISION == NIFTY_DOUBLE
        LogF(&config, 1, " for double precision complex visibility data.\n");
    #endif

    // Seed random number generator
    srand((unsigned) time(0));

    // Define where output files to be saved, ie: dirty image, intermediate w grids (testing only)
    strcpy(config.data_output_folder, "/home/seth/Desktop/Anthony/CUDA_Nifty/ska-gridder-nifty-cuda/output");

    strcpy(config.vis_intensity_file, "/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_vis");

    strcpy(config.vis_uvw_file, "/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_uvw");

    config.generating_psf = false; // uses nifty gridder with visibilities of 1.0+0.0i

    config.perform_shift_fft = true; // flag to (equivalently) rearrange each grid so origin is at lower-left corner for FFT

    config.upsampling = 2.0; // sigma, the scaling factor for padding each w grid (image_size * upsampling)

    config.image_size = 1024*1; // number of pixels in one dimension for dirty image

    config.grid_size = FLOOR(config.image_size * config.upsampling); // upscaled one dimension size of grid for each w grid

    config.num_visibilities = 175500; // 3924480 // How many visibilities to process

    config.support = 7; // full support for semicircle gridding kernel

    config.beta = (VIS_PRECISION) (2.307 * ((PRECISION)config.support)); // NOTE this 2.307 is only for when upsampling = 2

    config.fov = 20.0; // field of view in degrees

    config.freq = 1e8; // frequency of sampled visibility data (currently single chan only)

    config.num_channels = 1; // number of total channels (currently single chan only)

    // converts pixel index (x, y) to normalised image coordinate (l, m) where l, m between -0.5 and 0.5
    config.pixel_size = asin(2.0 * sin(0.5 * config.fov*PI/(180.0)) / PRECISION(config.image_size));

    // scaling factor for conversion of uv coords to grid coordinates (grid_size * cell_size)
    config.uv_scale = config.pixel_size * (PRECISION)config.grid_size * config.freq / C;

    // min/max abs w coord across all visibilities (in meters)
    //config.min_abs_w = 0.000605;
    //config.max_abs_w = 1770.405762;

	switch(config.num_visibilities)
	{
		case 175500:
			config.min_abs_w = 9.373007105751e-05;
			config.max_abs_w = 6.027269599466e+02;   // with 175500 visibilities
			break;
		case 2:
			config.min_abs_w = 2.149779005352e+01;
			config.max_abs_w = 4.632536897563e+01;
			break;
		case 5:
			config.min_abs_w = 2.149779005352e+01;
			config.max_abs_w = 1.596495948488e+02;
			break;
		case 10:
			config.min_abs_w = 3.143103496678e+00;
			config.max_abs_w = 3.231917769488e+02;
			break;
		case 100:
			config.min_abs_w = 3.143103496678e+00;
			config.max_abs_w = 3.574805404221e+02;
			break;
		case 1000:
			config.min_abs_w = 4.019969699916e-01;
			config.max_abs_w = 6.027269599466e+02;
			break;
		case 10000:
			config.min_abs_w = 2.655283906097e-02;
			config.max_abs_w = 6.027269599466e+02;
			break;
		case 100000:
			config.min_abs_w = 9.373007105751e-05;
			config.max_abs_w = 6.027269599466e+02;
			break;
		default:
			printf("ERROR: config.num_visibilities not recognised!!!\n");
			return 99;
	}
	
    LogF(&config, 2, "Min w: %e\n", config.min_abs_w);
    LogF(&config, 2, "Max w: %e\n", config.max_abs_w);

    config.num_w_grids_batched = 1; // How many w grids to grid at once on the GPU

    /****************************************************************************************
     * DO NOT MODIFY BELOW - DO NOT MODIFY BELOW - DO NOT MODIFY BELOW - DO NOT MODIFY BELOW 
     ***************************************************************************************/
    config.min_abs_w *= config.freq / C; // scaled from meters to wavelengths
    config.max_abs_w *= config.freq / C; // scaled from meters to wavelengths
    double x0 = -0.5 * config.image_size * config.pixel_size;
    double y0 = -0.5 * config.image_size * config.pixel_size;
    double nmin = sqrt(MAX(1.0 - x0*x0 - y0*y0, 0.0)) - 1.0;

    if (x0*x0 + y0*y0 > 1.0)
        nmin = -sqrt(abs(1.0 - x0*x0 - y0*y0)) - 1.0;

    config.w_scale = 0.25 / abs(nmin); // scaling factor for converting w coord to signed w grid index
    config.num_total_w_grids = (uint32_t) ((config.max_abs_w - config.min_abs_w) / config.w_scale + 2); //  number of w grids required
    config.w_scale = 1.0 / ((1.0 + 1e-13) * (config.max_abs_w - config.min_abs_w) / (config.num_total_w_grids - 1));
    config.min_plane_w = config.min_abs_w - (0.5*(PRECISION)config.support-1.0) / config.w_scale;
    config.max_plane_w = config.max_abs_w + (0.5*(PRECISION)config.support-1.0) / config.w_scale;
    config.num_total_w_grids += config.support - 2;
    /****************************************************************************************
     * DO NOT MODIFY ABOVE - DO NOT MODIFY ABOVE - DO NOT MODIFY ABOVE - DO NOT MODIFY ABOVE 
     ***************************************************************************************/

    LogF(&config, 2, "Min w: %f\n", config.min_abs_w);
    LogF(&config, 2, "Max w: %f\n", config.max_abs_w);
    LogF(&config, 2, "Min w plane: %f\n", config.min_plane_w);
    LogF(&config, 2, "Max w plane: %f\n", config.max_plane_w);
    LogF(&config, 2, "W scale: %f\n", config.w_scale);
    LogF(&config, 2, "Num planes: %d\n", config.num_total_w_grids);
    LogF(&config, 2, "Pixel size: %f\n", config.pixel_size);
    LogF(&config, 2, "UV SCALE IS: %f\n ", config.uv_scale);

    // Kicks off the nifty init/execute/clean pipeline
    execute_imaging_pipeline(&config);

    LogF(&config, 0, "Gridder ending...\n");
    return EXIT_SUCCESS;
}
